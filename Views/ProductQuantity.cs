namespace CustomerOrdersAPI.Views
{
    public class ProductQuantity
    {
        public string ProductName { get; set; }
        public decimal QuantitySold { get; set; }
    }
}