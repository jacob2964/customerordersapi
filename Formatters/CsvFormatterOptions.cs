// https://github.com/damienbod/AspNetCoreCsvImportExport

namespace CustomerOrdersAPI.Formatters
{
    public class CsvFormatterOptions
    {
        public bool UseSingleLineHeaderInCsv { get; set; } = true;

        public string CsvDelimiter { get; set; } = ",";

        public string Encoding { get; set; } = "ISO-8859-1";
    }
}
