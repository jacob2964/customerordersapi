using System;
using System.Net;
using CustomOrdersAPI.ErrorHandling;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;

public class CustomExceptionFilter : IExceptionFilter
{
    public void OnException(ExceptionContext context)
    {
        HttpStatusCode status = HttpStatusCode.InternalServerError;
        String message = String.Empty;

        var exceptionType = context.Exception.GetType();
        if (exceptionType == typeof(BadRequestException))
        {
            message = context.Exception.Message.ToString();
            status = HttpStatusCode.BadRequest;
        }
        else if (exceptionType == typeof(NotFoundException)) 
        {
            message = context.Exception.Message.ToString();
            status = HttpStatusCode.NotFound;
        }
        else
        {
            message = context.Exception.Message;
            status = HttpStatusCode.NotFound;
        }
        HttpResponse response = context.HttpContext.Response;
        response.StatusCode = (int)status;
        response.ContentType = "plain/text";
        response.WriteAsync(message);
    }
}