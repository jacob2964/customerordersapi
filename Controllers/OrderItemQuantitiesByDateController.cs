using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using CustomerOrdersAPI.Data;
using CustomerOrdersAPI.Models;
using CustomerOrdersAPI.Views;
using CustomOrdersAPI.ErrorHandling;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CustomOrdersAPI.Controllers
{
    [Route("api/orders/daterange")]
    public class OrderItemQuantitiesByDateController : Controller
    {
        private readonly CustomerOrdersDatabaseContext _context;
        private DateTime _startDate { get; set; }
        private DateTime _endDate { get; set; }
        private int? _day { get; set; }
        private int? _week { get; set; }
        private int? _month { get; set; }

        public OrderItemQuantitiesByDateController(CustomerOrdersDatabaseContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetOrders(string startDate, string endDate, string day, string week, string month)
        {
            ValidateDates(startDate, endDate);
            ValidateDayWeekMonth(day, week, month);
            return Ok(GetOrdersByDateRange());
        }

        [HttpGet]
        [Route("data.csv")]
        [Produces("text/csv")]
        public IActionResult GetOrdersCsv(string startDate, string endDate, string day, string week, string month)

        {
            ValidateDates(startDate, endDate);
            ValidateDayWeekMonth(day, week, month);
            return Ok(GetOrdersByDateRange());
        }

        private void ValidateDates(string startDate, string endDate)
        {
            if (startDate == null)
            {
                throw new BadRequestException("Please provide a start date.");
            }
            try
            {
                _startDate = DateTime.Parse(startDate);
            }
            catch (FormatException)
            {
                throw new BadRequestException("Please provide a valid startDate.");
            }

            if (endDate == null)
            {
                throw new BadRequestException("Please provide an endDate.");
            }
            try
            {
                _endDate = DateTime.Parse(endDate);
            }
            catch (FormatException)
            {
                throw new BadRequestException("Please provide a valid endDate.");
            }

            if (_startDate > _endDate)
            {
                throw new BadRequestException("Start date must come before end date.");
            };
        }

        private void ValidateDayWeekMonth(string day, string week, string month)
        {
            if (day == null && week == null && month == null)
            {
                throw new BadRequestException("Please provide a day, week, or month.");
            }

            if (day != null && week != null || day != null && month != null)
            {
                throw new BadRequestException ("You may only specify one of the following at a time: day, week, or month.");
            }

            if (week != null && month != null)
            {
                throw new BadRequestException ("You may only specify one of the following at a time: day, week, or month.");
            }

            if (day != null)
            {
                try
                {
                    _day = int.Parse(day);
                    if (_day > 7 || _day < 1)
                    {
                        throw new BadRequestException("Please provide a day int between 1 and 7.");
                    }
                }
                catch (FormatException)
                {
                    throw new BadRequestException("Please provide a valid day int.");
                }
            }

            if (week != null)
            {
                try
                {
                    _week = int.Parse(week);
                    if (_week > 53 || _week < 1)
                    {
                        throw new BadRequestException("Please provide a week int between 1 and 53.");
                    }
                }
                catch (FormatException)
                {
                    throw new BadRequestException("Please provide a valid week int.");
                }
            }

            if (month != null)
            {
                try
                {
                    _month = int.Parse(month);
                    if (_month > 12 || _month < 1)
                    {
                        throw new BadRequestException("Please provide a month int between 1 and 12.");
                    }
                }
                catch (FormatException)
                {
                    throw new BadRequestException("Please provide a valid month int.");
                }
            }
        }

        private IEnumerable<ProductQuantity> GetOrdersByDateRange()
        {
            if (_day != null)
            {
                var orders =  _context.Orders
                    .Where(o => o.OrderDate >= _startDate && o.OrderDate <= _endDate && (int)o.OrderDate.DayOfWeek + 1 == _day)
                    .Include("OrderItems")
                    .Include("OrderItems.Product")
                    .ToList();
                return GetProductQuantitiesForOrders(orders);
            }

            else if (_week != null)
            {
                var orders =  _context.Orders
                    .Where(o => o.OrderDate >= _startDate && o.OrderDate <= _endDate && _week == GetWeek(o.OrderDate))
                    .Include("OrderItems")
                    .Include("OrderItems.Product")
                    .ToList();
                return GetProductQuantitiesForOrders(orders);
            }

            else
            {
                var orders = _context.Orders
                    .Where(o => o.OrderDate >= _startDate && o.OrderDate <= _endDate && _month == o.OrderDate.Month)
                    .Include("OrderItems")
                    .Include("OrderItems.Product")
                    .ToList();
                return GetProductQuantitiesForOrders(orders);
            }
        }

        private int GetWeek(DateTime date)
        {
            var cultureInfo = new CultureInfo("en-US");
            var calendar = cultureInfo.Calendar;

            var calendarWeekRule = cultureInfo.DateTimeFormat.CalendarWeekRule;
            var firstDayOfWeek = cultureInfo.DateTimeFormat.FirstDayOfWeek;

            return calendar.GetWeekOfYear(date, calendarWeekRule, firstDayOfWeek);
        }

        private IEnumerable<ProductQuantity> GetProductQuantitiesForOrders(ICollection<Order> orders)
        {
            var products = new Dictionary<string, decimal>();
                foreach (var order in orders)
                {
                    foreach (var product in order.OrderItems)
                    {
                        if (products.ContainsKey(product.Product.Name))
                        {
                            products[product.Product.Name] += product.QuantitySold;
                        }
                        else
                        {
                            products.Add(product.Product.Name, product.QuantitySold);
                        }
                    }
                }
                var productQuantities = new List<ProductQuantity>();
                foreach(var key in products.Keys)
                {
                    productQuantities.Add(new ProductQuantity { ProductName = key, QuantitySold = products[key]});
                }
                return productQuantities;
        }
    }
}
