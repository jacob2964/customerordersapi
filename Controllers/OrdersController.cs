using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CustomerOrdersAPI.Models;
using System.Linq;
using System.Collections;
using CustomerOrdersAPI.Data;
using System.Net;
using System;
using CustomOrdersAPI.ErrorHandling;
using Microsoft.EntityFrameworkCore;

namespace CustomerOrdersAPI.Controllers
{
    [Route("api/[controller]")]
    public class OrdersController : Controller
    {
    private readonly CustomerOrdersDatabaseContext _context;

        private int _customerId { get; set;}
        public OrdersController(CustomerOrdersDatabaseContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetOrders(string customerId)
        {
            if (customerId == null)
            {
                return Ok(_context.Orders
                    .Include("Customer")
                    .Include("OrderItems")
                    .Include("OrderItems.Product")
                    .Include("OrderItems.Product.Categories")
                    .Include("Status"));
            }

            ValidateCustomerIdFilter(customerId);

            if (_context.Customers.FirstOrDefault(c => c.Id == _customerId) == null)
            {
                throw new NotFoundException("Customer not found.");
            }
            return Ok(_context.Orders
                .Where(o => o.CustomerId == _customerId)
                .Include("Customer")
                .Include("OrderItems")
                .Include("OrderItems.Product")
                .Include("OrderItems.Product.Categories")
                .Include("Status"));
        }

        private void ValidateCustomerIdFilter(string input)
        {
            try
            {
                _customerId = int.Parse(input);
            }
            catch (FormatException)
            {
                throw new BadRequestException("Please provide a valid customerId");
            }
        }
    }
}
