﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerOrdersAPI.Data;
using CustomerOrdersAPI.Formatters;
using CustomerOrdersAPI.Models;
using CustomOrdersAPI.Formatters.AspNetCoreCsvImportExport.Formatters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json.Converters;

namespace CustomerOrdersAPI
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddDbContext<CustomerOrdersDatabaseContext>(opt => opt.UseInMemoryDatabase("CustomerOrders"))
                .AddMvc(options =>
                {
                    options.Filters.Add(typeof(CustomExceptionFilter));
                    options.OutputFormatters.Add(new CsvOutputFormatter(new CsvFormatterOptions()));
                    options.FormatterMappings.SetMediaTypeMappingForFormat("csv", MediaTypeHeaderValue.Parse("text/csv"));
                })
                .AddJsonOptions(j => {
                    j.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                    j.SerializerSettings.Converters.Add(new IsoDateTimeConverter
                    {
                        DateTimeFormat = "yyyy-MM-dd"
                    });
                });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMvc();
        }
    }
}
