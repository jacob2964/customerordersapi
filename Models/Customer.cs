using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace CustomerOrdersAPI.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string FirstName {get;set;}
        public string LastName { get; set; }
    }
}
