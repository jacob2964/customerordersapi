namespace CustomerOrdersAPI.Models
{
    public class OrderItem
    {
        public int Id { get; set; }
        public Product Product { get; set; }
        public int OrderId { get; set; }
        public decimal QuantitySold { get; set; }
    }
}
