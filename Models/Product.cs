using System.Collections.Generic;

namespace CustomerOrdersAPI.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name {get;set;}
        public ICollection<Category> Categories { get; set;}
    }
}
