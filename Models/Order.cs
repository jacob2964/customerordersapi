using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System;
using Newtonsoft.Json;

namespace CustomerOrdersAPI.Models
{
    public class Order
    {
        [Key]
        public int Id { get; set; }

        [JsonIgnore]
        public int StatusId { get; set; }

        [JsonIgnore]
        public int CustomerId { get; set; }

        public DateTime OrderDate { get; set; }

        public ICollection<OrderItem> OrderItems { get; set; }

        [ForeignKey("StatusId")]
        public OrderStatus Status { get; set; }

        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }
    }
}
