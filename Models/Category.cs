using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace CustomerOrdersAPI.Models
{
    public class Category 
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}