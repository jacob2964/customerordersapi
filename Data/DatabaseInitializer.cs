using CustomerOrdersAPI.Models;
using System;
using System.Linq;
using System.Collections.Generic;

namespace CustomerOrdersAPI.Data
{
    public static class DatabaseInitializer
    {
        public static void Initialize(CustomerOrdersDatabaseContext context)
        {
            context.Database.EnsureCreated();

            if (context.OrderStatuses.Any())
            {
                return;
            }

            var orderStatuses = new OrderStatus[]
            {
                new OrderStatus { Name = "Waiting for delivery" },
                new OrderStatus { Name = "On its way" },
                new OrderStatus { Name = "Delivered" }
            };

            foreach (OrderStatus orderStatus in orderStatuses)
            {
                context.OrderStatuses.Add(orderStatus);
            }
            context.SaveChanges();

            var categories = new Category[]
            {
                new Category { Id = 1, Name = "Outdoor" },
                new Category { Id = 2, Name = "Cleaning" },
                new Category { Id = 3, Name = "Dessert" },
                new Category { Id = 4, Name = "Produce" },
                new Category { Id = 5, Name = "Snack" },
                new Category { Id = 6, Name = "Seafood" },
                new Category { Id = 7, Name = "Pet" }
            };

            foreach ( var category in categories)
            {
                context.Categories.Add(category);
            }

            var products = new Product[]
            {
                new Product { Id = 1, Name = "Charcoal", Categories = categories.Where(c => c.Id == 1).ToList() },
                new Product { Id = 2, Name = "Sushi", Categories = categories.Where(c => c.Id == 6).ToList() },
                new Product { Id = 3, Name = "Dog Food", Categories = categories.Where(c => c.Id == 7).ToList() },
                new Product { Id = 4, Name = "Chips", Categories = categories.Where(c => c.Id == 5).ToList() },
                new Product { Id = 5, Name = "Apple", Categories = categories.Where(c => c.Id == 4).ToList() }
            };

            foreach (Product product in products)
            {
                context.Products.Add(product);
            }
            context.SaveChanges();

            var orderItems = new OrderItem[]
            {
                new OrderItem { Product = products.First(p => p.Id == 1), OrderId = 1, QuantitySold = 10 },
                new OrderItem { Product = products.First(p => p.Id == 2), OrderId = 1, QuantitySold = 6 },
                new OrderItem { Product = products.First(p => p.Id == 3), OrderId = 1, QuantitySold = 12 },
                new OrderItem { Product = products.First(p => p.Id == 4), OrderId = 1, QuantitySold = 2 },
                new OrderItem { Product = products.First(p => p.Id == 5), OrderId = 1, QuantitySold = 4 },
                new OrderItem { Product = products.First(p => p.Id == 1), OrderId = 2, QuantitySold = 20 },
                new OrderItem { Product = products.First(p => p.Id == 1), OrderId = 3, QuantitySold = 10 },
                new OrderItem { Product = products.First(p => p.Id == 2), OrderId = 4, QuantitySold = 2 },
                new OrderItem { Product = products.First(p => p.Id == 4), OrderId = 4, QuantitySold = 4}
            };

            foreach (var orderItem in orderItems)
            {
                context.OrderItems.Add(orderItem);
            }
            context.SaveChanges();

            var orders = new Order[]
            {
                new Order { CustomerId = 1, StatusId = 1, OrderItems = orderItems.Where(oi => oi.OrderId == 1).ToList(),
                    OrderDate = new DateTime(2018, 1, 1) },
                new Order { CustomerId = 1, StatusId = 1, OrderItems = orderItems.Where(oi => oi.OrderId == 2).ToList(),
                    OrderDate = new DateTime(2018, 1, 2) },
                new Order { CustomerId = 2, StatusId = 2, OrderItems = orderItems.Where(oi => oi.OrderId == 3).ToList(),
                    OrderDate = new DateTime(2018, 1, 15) },
                new Order { CustomerId = 3, StatusId = 3, OrderItems = orderItems.Where(oi => oi.OrderId == 4).ToList(),
                    OrderDate = new DateTime(2018, 1, 26) },
                
            };

            foreach (Order order in orders)
            {
                context.Orders.Add(order);
            }
            context.SaveChanges();

            var customers = new Customer[]
            {
                new Customer { FirstName = "Jacob", LastName = "Johnson" },
                new Customer { FirstName = "Heather", LastName = "Johnson" },
                new Customer { FirstName = "Ron", LastName = "Burgandy" }
            };

            foreach (Customer customer in customers)
            {
                context.Customers.Add(customer);
            }
            context.SaveChanges();
        }
    }
}
