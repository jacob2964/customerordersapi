using CustomerOrdersAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace CustomerOrdersAPI.Data
{

    public class CustomerOrdersDatabaseContext : DbContext
    {
        public CustomerOrdersDatabaseContext(DbContextOptions<CustomerOrdersDatabaseContext> options) : base(options)
        {
        }

        public DbSet<Customer> Customers  { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }

    }
}
